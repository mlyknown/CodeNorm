
# CSS编码规范

##基础部分  https://github.com/fex-team/styleguide/blob/master/css.md

##补充


*1 命名规范

*2 附录

###2 命名规范

###2.1 通用css命名规范

> 采用属性+值得方式

```css
/*宽度为64px*/
.w64 {
    width: 64px;
}
```

#2.2 一般css命名

> id和class命名约定

```css
/*
id 和 class 的命名基本原则:内容优先，表现为辅。首先根据内容来命名，如:#header,#footer,.main-nav.如根据内容无法找到合适的命名，可以再结合表现进行命名，如：col-main, col-sub, col-extra,blue-box
id 和 class 的名称一律小写，多个单词以连字符连接，如： main-wrap
id 和 class 的名称只能出现，小写字母，数字和连字符( - )(js钩子除外)
id 和 class 的名称尽量使用英文单词命名,如确实找不到合适的单词，可以使用拼音，如：zhidao-com
在不影响语意的情况下，id 和 class 的名称 可以适当使用缩写，如: col, nav, hd, bd, fd(缩写只用来表示结构，不允许写任何样式)。不要自造缩写。
class 对于选中命名.selected;对于hover，支持伪类使用:hover，不支持的使用 .hover，隐藏使用.hide 。
id 和 class 的选择，如果只使用一次，使用id,如果使用多次使用.class，如果需要和js交互，使用id,如果需要交互并且页面中有大量重复，请参见下一条。
对于作为js钩子的 id 和 class 命名规则为以”J_“开头(J,象形钩子的形状)，后面加上原应有的命名，在使用class的时候需要放在最前面。如:class="J_tab-content some-mod-content"。（注意：钩子，不允许在css中定义任何的样式效果）
很多浏览器会将含有这些词的作为广告拦截： ad、ads、adv、banner、sponsor、gg、guangg、guanggao等 页面中尽量避免采用以上词汇来命名。
*/
```

###2.2

> 通用命名

```css
(1)页面框架命名，一般具有唯一性，推荐用ID命名

ID名称    命名  ID名称    命名
头部  header  主体  main
脚部  footer  容器  wrapper
侧栏  sideBar 栏目  column
布局  layout      
(2)模块结构命名

Class名称 命名  Class名称 命名
模块(如：新闻模块)  mod (mod-news)  标题栏 title
内容  content 次级内容    sub-content
(3)导航结构命名

Class名称 命名  Class名称 命名
导航  nav 主导航 main-nav
子导航 sub-nav 顶部导航    top-nav
菜单  menu    子菜单 sub-menu
(4)一般元素命名

Class名称 命名  Class名称 命名
二级  sub 面包屑 breadcrumb
标志  logo    广告  bner(禁用banner或ad)
登陆  login   注册  register/reg
搜索  search  加入  join
状态  status  按钮  btn
滚动  scroll  标签页 tab
文章列表    list    短消息 msg/message
当前的 current 提示小技巧   tips
图标  icon    注释  note
指南  guide   服务  service
热点  hot 新闻  news
下载  download    投票  vote
合作伙伴    partner 友情链接    link
版权  copyright   演示  demo
下拉框 select  摘要  summary
翻页  pages   主题风格    themes
设置  set 成功  suc
按钮  btn 文本  txt
颜色  color/c 背景  bg
边框  border/bor  居中  center
上   top/t   下   bottom/b
左   left/l  右   right/r
添加  add 删除  del
间隔  sp  段落  p
弹出层 pop 公共  global/gb
操作  op  密码  pwd
透明  tran    信息  info
重点  hit 预览  pvw
单行输入框   input   首页  index
日志  blog    相册  photo
留言板 guestbook   用户  user
确认  confirm 取消  cancel
报错  error       
(5)全局皮肤样式

文字颜色(命名空间text-xxx)

text-c1, text-c2,text-c3……

背景颜色(命名空间bg -xxx)

bg-c1,bg-c2,bg-c3……

边框颜色(命名空间border-xxx)

border-c1,border-c2,border-c3……
.c{

}
```


##2 附录

>CSS规范 - 最佳实践

```css
最佳选择器写法（模块）
/* 这是某个模块 */
.m-nav{}/* 模块容器 */
.m-nav li,.m-nav a{}/* 先共性  优化组合 */
.m-nav li{}/* 后个性  语义化标签选择器 */
.m-nav a{}/* 后个性中的共性 按结构顺序 */
.m-nav a.a1{}/* 后个性中的个性 */
.m-nav a.a2{}/* 后个性中的个性 */
.m-nav .z-crt a{}/* 交互状态变化 */
.m-nav .z-crt a.a1{}
.m-nav .z-crt a.a2{}
.m-nav .btn{}/* 典型后代选择器 */
.m-nav .btn-1{}/* 典型后代选择器扩展 */
.m-nav .btn-dis{}/* 典型后代选择器扩展（状态） */
.m-nav .btn.z-dis{}/* 作用同上，请二选一（如果可以不兼容IE6时使用） */
.m-nav .m-sch{}/* 控制内部其他模块位置 */
.m-nav .u-sel{}/* 控制内部其他元件位置 */
.m-nav-1{}/* 模块扩展 */
.m-nav-1 li{}
.m-nav-dis{}/* 模块扩展（状态） */
.m-nav.z-dis{}/* 作用同上，请二选一（如果可以不兼容IE6时使用） */
统一语义理解和命名
布局（.g-）
语义  命名  简写
文档  doc doc
头部  head    hd
主体  body    bd
尾部  foot    ft
主栏  main    mn
主栏子容器   mainc   mnc
侧栏  side    sd
侧栏子容器   sidec   sdc
盒容器 wrap/box    wrap/box
模块（.m-）、元件（.u-）
语义  命名  简写
导航  nav nav
子导航 subnav  snav
面包屑 crumb   crm
菜单  menu    menu
选项卡 tab tab
标题区 head/title  hd/tt
内容区 body/content    bd/ct
列表  list    lst
表格  table   tb
表单  form    fm
热点  hot hot
排行  top top
登录  login   log
标志  logo    logo
广告  advertise   ad
搜索  search  sch
幻灯  slide   sld
提示  tips    tips
帮助  help    help
新闻  news    news
下载  download    dld
注册  regist  reg
投票  vote    vote
版权  copyright   cprt
结果  result  rst
标题  title   tt
按钮  button  btn
输入  input   ipt
功能（.f-）
语义  命名  简写
浮动清除    clearboth   cb
向左浮动    floatleft   fl
向右浮动    floatright  fr
内联块级    inlineblock ib
文本居中    textaligncenter tac
文本居右    textalignright  tar
文本居左    textalignleft   tal
垂直居中    verticalalignmiddle vam
溢出隐藏    overflowhidden  oh
完全消失    displaynone dn
字体大小    fontsize    fs
字体粗细    fontweight  fw
皮肤（.s-）
语义  命名  简写
字体颜色    fontcolor   fc
背景  background  bg
背景颜色    backgroundcolor bgc
背景图片    backgroundimage bgi
背景定位    backgroundposition  bgp
边框颜色    bordercolor bdc
状态（.z-）
语义  命名  简写
选中  selected    sel
当前  current crt
显示  show    show
隐藏  hide    hide
打开  open    open
关闭  close   close
出错  error   err
不可用 disabled    dis
```


著作权归作者所有。
商业转载请联系作者获得授权，非商业转载请注明出处。
作者：徐尤熙
链接：https://www.zhihu.com/question/38773260/answer/78012583
来源：知乎

二、相对网页外层重要部分CSS样式命名外套 wrap ------------------用于最外层头部 header ---------------用于头部主要内容 main ------------用于主体内容（中部）左侧 main-left -------------左侧布局右侧 main-right -----------右侧布局导航条 nav -----------------网页菜单导航条内容 content --------------用于网页中部主体底部 footer -----------------用于底部三、DIV+CSS命名参考表以下为CSS样式命名与CSS文件命名参考表，DIV CSS命名集合：CSS样式命名说明网页公共命名#wrapper页面外围控制整体布局宽度#container或#content容器,用于最外层#layout布局#head, #header页头部分#foot, #footer页脚部分#nav主导航#subnav二级导航#menu菜单#submenu子菜单#sideBar侧栏#sidebar_a, #sidebar_b左边栏或右边栏#main页面主体#tag标签#msg #message提示信息#tips小技巧#vote投票#friendlink友情连接#title标题#summary摘要#loginbar登录条#searchInput搜索输入框#hot热门热点#search搜索#search_output搜索输出和搜索结果相似#searchBar搜索条#search_results搜索结果#copyright版权信息#branding商标#logo网站LOGO标志#siteinfo网站信息#siteinfoLegal法律声明#siteinfoCredits信誉#joinus加入我们#partner合作伙伴#service服务#regsiter注册arr/arrow箭头#guild指南#sitemap网站地图#list列表#homepage首页#subpage二级页面子页面#tool, #toolbar工具条#drop下拉#dorpmenu下拉菜单#status状态#scroll滚动.tab标签页.left .right .center居左、中、右.news新闻.download下载.banner广告条(顶部广告条)电子贸易相关.products产品.products_prices产品价格.products_description产品描述.products_review产品评论.editor_review编辑评论.news_release最新产品.publisher生产商.screenshot缩略图.faqs常见问题.keyword关键词.blog博客.forum论坛CSS文件命名说明master.css,style.css主要的module.css模块base.css基本共用layout.css布局，版面themes.css主题columns.css专栏font.css文字、字体forms.css表单mend.css补丁print.css打印CSS命名其它说明：DIV+CSS命名小结：无论是使用“.”（小写句号）选择符号开头命名，还是使用“#”(井号)选择符号开头命名都无所谓，但我们最好遵循，主要的、重要的、特殊的、最外层的盒子用“#”(井号)选择符号开头命名，其它都用“.”（小写句号）选择符号开头命名，同时考虑命名的CSS选择器在HTML中重复使用调用。通常我们最常用主要命名有：wrap（外套、最外层）、header（页眉、头部）、nav(导航条)、menu(菜单)、title(栏目标题、一般配合h1\h2\h3\h4标签使用)、content (内容区)、footer(页脚、底部)、logo（标志、可以配合h1标签使用）、banner（广告条，一般在顶部）、copyRight（版权）。其它可根据自己需要选择性使用。建议：主要的、重要的、最外层的盒子用“#”(井号)选择符号开头命名，其它都用“.”（小写句号）选择符号开头命名。2.CSS样式文件命名如下：主要的 master.css布局，版面 layout.css专栏 columns.css文字 font.css打印样式 print.css主题 themes.css



##重要
常用的CSS命名规则

头：header
内容：content/container
尾：footer
导航：nav
侧栏：sidebar
栏目：column
页面外围控制整体佈局宽度：wrapper
左右中：left right center
登录条：loginbar
标志：logo
广告：banner
页面主体：main
热点：hot
新闻：news
下载：download
子导航：subnav
菜单：menu
子菜单：submenu
搜索：search
友情链接：friendlink
页脚：footer
版权：copyright
滚动：scroll
内容：content
标签：tags
文章列表：list
提示信息：msg
小技巧：tips
栏目标题：title
加入：joinus
指南：guide
服务：service
注册：regsiter
状态：status
投票：vote
合作伙伴：partner

注释的写法:

/* Header */

id的命名:

1)页面结构

容器: container
页头：header
内容：content/container
页面主体：main
页尾：footer
导航：nav
侧栏：sidebar
栏目：column
页面外围控制整体佈局宽度：wrapper
左右中：left right center

(2)导航

导航：nav
主导航：mainnav
子导航：subnav
顶导航：topnav
边导航：sidebar
左导航：leftsidebar
右导航：rightsidebar
菜单：menu
子菜单：submenu
标题: title
摘要: summary

(3)功能

标志：logo
广告：banner
登陆：login
登录条：loginbar
注册：register
搜索：search
功能区：shop
标题：title
加入：joinus
状态：status
按钮：btn
滚动：scroll
标籤页：tab
文章列表：list
提示信息：msg
当前的: current
小技巧：tips
图标: icon
注释：note
指南：guild
服务：service
热点：hot
新闻：news
下载：download
投票：vote
合作伙伴：partner
友情链接：link
版权：copyright

注意事项::

1.一律小写;
2.尽量用英文;
3.不加中槓和下划线;
4.尽量不缩写，除非一看就明白的单词。

CSS样式表文件命名

主要的 master.css
模块 module.css
基本共用 base.css
布局、版面 layout.css
主题 themes.css
专栏 columns.css
文字 font.css
表单 forms.css
补丁 mend.css
打印 print.css