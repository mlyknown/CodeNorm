


# EcmaScript6 全规范
##基础部分 https://github.com/ouvens/es6-code-style-guide

* 1、vuejs一些规范
* 2、项目规范


## 1、vuejs规范
> 导入模块命名

```javascript
//good
import ConfirmOrderPage from "./confirm-order.vue"
```

## 2、微信点单项目规范
>文件命名方式

```javascript
//按照项目功能命名按照短横分隔命名(kebab-case)
confirm-order.vue
//通用组件按照组件类型命名按照短横分隔命名(kebab-case)
cell-body.vue
```
